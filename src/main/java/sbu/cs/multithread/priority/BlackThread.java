package sbu.cs.multithread.priority;

import java.util.concurrent.CountDownLatch;

public class BlackThread extends ColorThread {

    private static final String MESSAGE = "hi blues, hi whites";

    public BlackThread(CountDownLatch colorLatch) {
        super(colorLatch);
    }


    void printMessage() {
        super.printMessage(new Message(this.getClass().getName(), getMessage()));
    }

    @Override
    String getMessage() {
        return MESSAGE;
    }

    @Override
    public void run() {
        // call printMessage
        printMessage();
        count();
    }
}
