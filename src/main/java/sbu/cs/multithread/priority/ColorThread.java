package sbu.cs.multithread.priority;
import java.util.concurrent.CountDownLatch;

public abstract class ColorThread extends Thread {
    CountDownLatch colorLatch;
    private CountDownLatch singleLatch;
    public ColorThread(CountDownLatch colorLatch) {
        this.colorLatch = colorLatch;
        singleLatch = new CountDownLatch(1);
    }

    void printMessage(Message message) {
        System.out.printf("[x] %s. thread_name: %s%n", message.toString(), currentThread().getName());
        Runner.addToList(message);
    }

    abstract String getMessage();

    public void count() {
        colorLatch.countDown();
        singleLatch.countDown();
    }
    public CountDownLatch getSingleLatch() {
        return singleLatch;
    }
}