package sbu.cs.exception;

public class BadInput extends ApException{
    static final String EXCEPTION_MESSAGE = "< Bad Input  !>";

    public BadInput() {
        super(EXCEPTION_MESSAGE);
    }
}
