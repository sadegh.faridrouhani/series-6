package sbu.cs.exception;

public class NotImplementedCommand extends ApException{
    static final String EXCEPTION_MESSAGE = "<Not Implemented Command !>";

    public NotImplementedCommand() {
        super(EXCEPTION_MESSAGE);
    }
}
