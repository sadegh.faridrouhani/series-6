package sbu.cs.exception;

public class UnrecognizedCommand extends ApException{
    static final String EXCEPTION_MESSAGE = "< Unrecognized Command !>";

    public UnrecognizedCommand() {
        super(EXCEPTION_MESSAGE);
    }


}
